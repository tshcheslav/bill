//
//  SecondViewController.swift
//  BillDevider
//
//  Created by 1 on 16.11.2021.
//

import UIKit

class SecondViewController: UIViewController {

    var result = 0.0
    
    @IBOutlet weak var resultLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.resultLabel.text = "\(result)"
    }
    


}
