//
//  ViewController.swift
//  BillDevider
//
//  Created by 1 on 15.11.2021.
//

import UIKit

class ViewController: UIViewController {

    var tip: Double = 0
    var peopleCount = 2
    var result = 0.0
    
    @IBOutlet weak var peopleCountLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func tipButtonPressed(_ sender: UISegmentedControl) {
        var index = 0
        index = sender.selectedSegmentIndex
        let title = sender.titleForSegment(at: index)

        let tip = Double(title!.dropLast())!/100
             self.tip = tip
    }
    

    
    @IBAction func peopleStepper(_ sender: UIStepper) {
        peopleCountLabel.text = String(format: "%.0f", sender.value)
        self.peopleCount = Int(String(format: "%.0f", sender.value))!
    }
    
    @IBAction func calcPressed(_ sender: UIButton) {
        self.result =  Double(textField.text!)! * (1 + self.tip) / Double(self.peopleCount)
        print(result)
        performSegue(withIdentifier: "go", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "go" {
            guard let vc = segue.destination as? SecondViewController else {return}
            vc.result = self.result
        }
    }
    
}

