//
//  ViewController.swift
//  myCulc
//
//  Created by 1 on 19.11.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textlabel: UILabel!
    var first = 0.0
    var second = 0.0
    var proverka = false
    var operation: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
//       textlabel.text = "0"
    }

    @IBAction func digits(_ sender: UIButton) {
        let tagButton = sender.titleLabel?.text
        if tagButton == "0" && textlabel.text == "0" {
            proverka = false
        } else {
            if proverka == true || textlabel.text! == "0" {
            textlabel.text = tagButton!
            proverka = false
        } else {
            textlabel.text = textlabel.text! + tagButton!
        }
        }
        second = Double(textlabel.text!)!
    }
    
    
    @IBAction func actionsButton(_ sender: UIButton) {
        let tagButton = sender.tag
        
        if textlabel.text != "" && tagButton != 10 && tagButton != 17 {
            first = Double(textlabel.text!)!
        }
        
        if tagButton == 10 {
            textlabel.text = ""
        }
        
        
        
        if tagButton == 13 {
            textlabel.text = "/"
            operation = tagButton
            proverka = true
        }else if tagButton == 14 {
            textlabel.text = "*"
            operation = tagButton
            proverka = true
        } else if tagButton == 15 {
            textlabel.text = "-"
            operation = tagButton
            proverka = true
        } else if tagButton == 16 {
            textlabel.text = "+"
            operation = tagButton
            proverka = true
        } else if tagButton == 17 {
            if operation == 13 && second == Double("0") {
                textlabel.text = "0"
            } else {
            if operation == 13 {
                var res = String(second)
                if res.contains("%") {
                    res.removeLast()
                    print(res)
                    textlabel.text = String(first / (first * Double(res)! / 100 ))
                    
                } else {
                    print(second)
                    textlabel.text = String(first / Double(res)!)
                }
            } else if operation == 14 {
                var res = String(second)
                if res.hasSuffix("%") {
                    res.removeLast()
                    textlabel.text = String(first * (first * Double(res)! / 100 ))
                } else {
                    textlabel.text = String(first * second) }
            } else if operation == 15 {
                var res = String(second)
                if res.hasSuffix("%") {
                    res.removeLast()
                    textlabel.text = String(first - (first * Double(res)! / 100) )
                } else {
                    textlabel.text = String(first - second) }
            } else if operation == 16 {
                var res = String(second)
                if res.hasSuffix("%") {
                    res.removeLast()
                    textlabel.text = String(first + (first  * Double(res)!  ))
                } else {
                    textlabel.text = String(first + second) }
            }
            }


        }
    }
    

    
    @IBAction func plusMinusAction(_ sender: UIButton) {
        if textlabel.text != "" {
            var value = ""
            if textlabel.text!.hasPrefix("-") {
                value = textlabel.text!
                value.removeFirst()
                textlabel.text = String(value)


            } else {
                value =  "-" + textlabel.text!
                textlabel.text = value

            }

        }


    }
    
    
    
    @IBAction func precent(_ sender: UIButton) {
        let value = textlabel.text
        if !(value?.contains("%"))!{
            if textlabel.text != "" && textlabel.text != "0" {
                textlabel.text = textlabel.text! + "%"
            }
        }
    }
    
    
    @IBAction func doatAddAction(_ sender: UIButton) {
        let value = textlabel.text
        if !(value?.contains("."))! {
            textlabel.text = textlabel.text! + "."
        }
    }
    
    
}

